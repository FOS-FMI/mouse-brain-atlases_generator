import os
import nrrd
import numpy as np
import pandas as pd
import copy
import matplotlib.pyplot as plt
import math
import nibabel as nib
import sys

def divide(fn_input_AV_ori, fn_output_AV_update, annotation_str):
	"""
	Assigns the right side of brain in abi 10 micron nrrd file new separate id values and adjusts abi annotation accordingly 
	
	Parameters
	----------
	
	Returns
	-------
	
	Notes
	---
	Code from  https://www.nature.com/articles/s41598-021-85807-0
	"""
	# detailed ROI using data for gene expression and fiber innervation

	nrrd_file = os.path.abspath(os.path.expanduser(fn_input_AV_ori))

	readnrrd = nrrd.read(nrrd_file)
	AV_ori = readnrrd[0]
	header = readnrrd[1]

	id_spacer = 2*10**9 # this number is added to ID in the right hemisphere of annotation volume
#id_root_peri = 1000000028 # For deleting a ROI for root_peri

#AV_ori, header = nrrd.read(os.path.abspath(os.path.expanduser('abi_10micron_annotation.nrrd')))

	AV_update = copy.deepcopy(AV_ori)
	AV_update[:,:,-570:] = AV_ori[:,:,-570:] + id_spacer #100 micron nrrd file -- adjust 57
	AV_update[AV_ori == 0] = 0
#AV_update[AV_ori == id_root_peri] = 0

	nrrd.write(os.path.abspath(os.path.expanduser(fn_output_AV_update)), AV_update, header)

	annotation_file=os.path.abspath(os.path.expanduser(annotation_str))

	startinglines=[]
	lines=[]
	newlines=[]
	endinglines=[]


	new_ids=0
	line_index=0
	with open(annotation_file) as annotation:
		for line in annotation:
			if(line.find("\"id\": ")!=-1):
				start = line.find("\": ") + len("\": ")
				end = line.find(",")
				original = line[start:end]
				new=int(original)+(2*10**9)
				new=str(new)
				newline=line.replace(original, new)
		   # print(new)
		   # print(line)
			elif(line.find("\"name\": ")!=-1):
				end = line.find("\",")
				newline=line[:end]+" right"+"\",\n"
				line=line[:end]+" left"+"\",\n"
			else:
				newline=line

			if(line_index==17546):
				line="},\n"


			if(line_index<7):
				startinglines.append(line)
			elif(line_index>17546):
				endinglines.append(line)
			else:
				lines.append(line)
				newlines.append(newline)

			line_index=line_index+1

	finalLines=startinglines+lines+newlines+endinglines

	with open(annotation_file, "w+") as f:
		for i in finalLines:
			f.write(i)
