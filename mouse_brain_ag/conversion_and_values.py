import os
import nrrd
import nibabel
import numpy as np
import sys
import math
import fileinput


def replace_with_dict2(ar, dic):
	"""
	Replaces all values in arr with corresponding values in dic, where keys are the original values

	----

	Parameters:
	ar: numpy array
	dic: dict

	---
	Returns:
	numpy array

	---
	Notes:
	"""
	# Extract out keys and values
	k = np.array(list(dic.keys()))
	v = np.array(list(dic.values()))

	# Get argsort indices
	sidx = k.argsort()

	ks = k[sidx]
	vs = v[sidx]
	return vs[np.searchsorted(ks,ar)]

def labels_nrrd_to_nifti(nrrd_file, nifti_file, annotation_file):
	"""
	Converts abi labes from nifti to nrrd
	----

	Parameters:
	None

	---
	Returns:
	None

	---
	Notes:
	Adjusts id values in labels and annotation file to decrease max magnitudes of id numbers
	"""

	path = os.path.abspath('.')

	print("Reading " + nrrd_file)
	readnrrd = nrrd.read(nrrd_file)
	data = readnrrd[0]
	header = readnrrd[1]

	print("Creating dictionary")
	data_flat=data.flatten()
	values = set(data_flat)
	values = sorted(list(values))

	values_length = len(values)
	new_values = list(range(values_length))

	values_dict = dict(zip(values,new_values))

	#edit annotations based on dict

	print("Editing annotation (json) file")


	lines=[]

	new_ids=0

	with open(annotation_file) as annotation:
		for line in annotation:
			if(line.find("\"id\": ")!=-1):
		   # print(line)
		   # x = line.split("\": ")
				start = line.find("\": ") + len("\": ")
				end = line.find(",")
				original = line[start:end]
				if int(original) in values_dict.keys():
					new = str(values_dict[int(original)])

				else:
					new =str(values_length+new_ids)
					new_ids=new_ids+1

				line=line.replace(original, new)
			lines.append(line)

	with open(annotation_file, "w+") as f:
		for i in lines:
			f.write(i)


	print("Altering nrrd data")
	#edit data based on dict

	new_data = replace_with_dict2(data,values_dict)

	data=new_data.astype(np.int16)

	# value conversion goes here, new_data will be the product, a 3D array

	affine_matrix = np.array(header["space directions"],dtype=np.float)
#if space[0] == 'left':
#	affine_matrix[0,0] = affine_matrix[0,0] * (-1)
#if space[1] == 'posterior':
#	affine_matrix[1,1] = affine_matrix[1,1] * (-1)
#Units?
	affine_matrix = affine_matrix*0.001

	affine_matrix = np.insert(affine_matrix,3,[0,0,0], axis=1)
	affine_matrix = np.insert(affine_matrix,3,[0,0,0,1], axis=0)

	#Change Orientation from PIR to RAS. Steps: PIR -> RIP -> RPI -> RPS -> RAS
	data.setflags(write=1)
	data = np.swapaxes(data,0,2)
	data = np.swapaxes(data,1,2)
	data = data[:,:,::-1]
	data = data[:,::-1,:]
#
	img = nibabel.Nifti1Image(data,affine_matrix)
	nibabel.save(img,nifti_file)
#
	#Delete Nrrd-File
	os.remove(nrrd_file)

