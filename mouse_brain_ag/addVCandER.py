import json
import numpy as np
import nibabel as nib
from scipy import ndimage
from jsonfunctions import find_id, ischildof, trim_zeros, get_target_list, create_binary_array, effective_resolution, volume_childless, volume_branch

#reads through labels file and edits annotation to contain volume of each structure and effective resolution


def addVCandER(infile_str="abi_annotation.json", outfile_str="abi_annotation.json", labels_str="abi2dsurqec_15micron_labels.nii):
  """
   reads through labels file and edits annotation to contain volume of each structure and effective resolution

   Parameters
   ----------
   in_file_str: optional, file name of json file read
   out_file_str: optional, file name for completely altered json

   Notes
   ------


  """
   print("loading data")

   path = os.path.abspath('.')

   infile=os.path.abspath(os.path.expanduser(infile_str))
   outfile=os.path.abspath(os.path.expanduser(outfile_str))

   image_15 = nib.load(os.path.abspath(os.path.expanduser(labels_str)))
   #image_40 = nib.load(os.path.abspath(os.path.expanduser('abi2dsurqec_40micron_labels.nii')))
   #image_200 = nib.load('abi2dsurqec_200micron_labels.nii')
   
   # to be extra sure of not overwriting data:
   new_data_15 = image_15.get_fdata()
   data_flat_15=new_data_15.flatten()

  # new_data_40 = image_40.get_fdata()
  # data_flat_40=new_data_40.flatten()

   #new_data_200 = image_200.get_fdata()
   #data_flat_200=new_data_200.flatten()

   f = open(infile,)
   json_file = json.load(f)

   count=0
   print("add volume")


   #find volume of all childless structures ("nodes")
   for i in range(len(json_file['msg'])):
      if(len(json_file['msg'][i]['children'])==0):
     #    print(".", end="")
         #count+=1
         ID=json_file['msg'][i]['id']
         json_file['msg'][i]['volume']=volume_childless(ID, data_flat_15, 15)
      #if(count==49):
         #print()
         #count=0

#find volume of structures with children ("branches")
   for i in range(len(json_file['msg'])):
      if(len(json_file['msg'][i]['children'])!=0):
         ID=json_file['msg'][i]['id']
         json_file['msg'][i]['volume']=volume_branch(json_file, ID)

   #print()
   print("calculating effective resolution")
   for i in range(len(json_file['msg'])):
      #print(".", end="")
      #if(i%50==0):
         #print()
      l=get_target_list(json_file['msg'][i]['id'], json_file, data_flat_15)
      arr=create_binary_array(l, new_data_15)
      er=effective_resolution(arr, 15)
      json_file['msg'][i]['effective resolution']=er

   with open(outfile, 'w+') as f:
      json.dump(json_file, f, indent=4)
