from jsonfunctions import ischildof, find_id, depth
import json
import numpy as np
from itertools import chain as it
#code to test using annotation to determine parcellation given resolution


#d={1:2}
#print(set(d))


def find_structures_with_resolution(resolution, filename="abi_annotation.json"):
	"""
	Generates list of structures that are visible at given resolution (in µm)
	
	Parameters
	----------
	resolution: int or float
	filename: str
	
	Returns
	-------
	List of all structures visible at resolution
	
	Notes
	-----
	filename must be name of annotation file with effective resolution values
	"""

	f = open(os.path.abspath(os.path.expanduser(filename)),)
	json_file = json.load(f)

	l=[x['id'] for x in json_file['msg'] if len(x['children'])==0]


	for i in l:
		structure=i
		cont=True

		while(cont):
			if(json_file['msg'][find_id(structure, json_file)]['effective resolution']>=resolution):
				cont=False
			else:
				cont=True
				structure=json_file['msg'][find_id(ischildof(structure, json_file), json_file)]['id']
				l=list(set(l)-set(json_file['msg'][find_id(structure, json_file)]['children'])) 
				l.append(structure)

	l=list(set(l))
	return l

