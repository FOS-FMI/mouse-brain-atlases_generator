import nibabel as nib
import numpy as np
import json
from jsonfunctions import ischildof, find_id, depth, get_target_list
from conversion_and_values import replace_with_dict2
import os

def find_structures_with_resolution(resolution, filename="abi_annotation.json"):
	"""
	Generates list of structures that are visible at given resolution (in µm)
	
	Parameters
	----------
	resolution: int or float
		resolution to filter structures at
	filename: str
		directory of annotation file

	Returns
	-------
	List of all structures visible at resolution
	
	Notes
	-----
	filename must be name of annotation file with effective resolution values
	"""

	f = open(os.path.abspath(os.path.expanduser(filename)),)
	json_file = json.load(f)

	l=[x['id'] for x in json_file['msg'] if len(x['children'])==0]


	for i in l:
		structure=i
		cont=True

		while(cont):
			if(int(json_file['msg'][find_id(structure, json_file)]['effective resolution'])>=resolution):
				cont=False
			else:
				cont=True
				structure=json_file['msg'][find_id(ischildof(structure, json_file), json_file)]['id']
				l=list(set(l)-set(json_file['msg'][find_id(structure, json_file)]['children']))
				l.append(structure)

	l=list(set(l))
	return l
 
def create_nifti(labels_str, annotation_str, output_str, resolution):
	"""
	creates nifti file of abi labels that is parcellated to level of detail for given resolution

	Parameters
	----------
	labels_str: str
		directory of labels file (choose either the existing 15µm, 40µm, or 200µm)

	annotation_str: str
		directory of annotation file

	output_str: str
		directory of output labels file

	resolution: int or float
		resolution to filter structures at

	Returns
	-------
	None
	
	Notes
	-----
	filename must be name of annotation file with effective resolution values
	"""
	image= nib.load(os.path.abspath(os.path.expanduser(labels_str)))
	hd=image.header
	data = image.get_fdata()
	data_flat=data.flatten()

	f = open(os.path.abspath(os.path.expanduser(annotation_str,)))
	json_file = json.load(f)

	print("Getting structures...")
	list_structures=find_structures_with_resolution(resolution, annotation_str)

	structures_dict={}

	for i in list_structures:
		l=get_target_list(i,json_file, data_flat)
		for j in l:
			structures_dict[j]=i

	print("Adjusting nifti data")

	new_data = replace_with_dict2(data,structures_dict)

	if hd['sizeof_hdr'] == 348:
		new_image = nib.Nifti1Image(new_data, image.affine, header=hd)
	# if nifty2
	elif hd['sizeof_hdr'] == 540:
		new_image = nib.Nifti2Image(new_data, image.affine, header=hd)
	else:
		raise IOError('Input image header problem')

	nib.save(new_image, os.path.abspath(os.path.expanduser(output_str)))
