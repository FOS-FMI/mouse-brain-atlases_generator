import nibabel as nib
import numpy as np
import os

def adjust_15micronlabels(image_dir):
	"""
	Converts data type of 15 micron nifti labels file produced by AntsRegistration call from int32 to int16 to size of 15 micron nifti files
	
	Parameters
	----------
	
	Returns
	-------
	
	Notes
	---
	"""
	path = os.path.abspath('.')


	image = nib.load(os.path.abspath(os.path.expanduser(image_dir)))



	# to be extra sure of not overwriting data:
	new_data = np.copy(image.get_data())
	hd = image.header

	# in case you want to remove nan:
	#new_data = np.nan_to_num(new_data)

	# update data type:
	new_dtype = np.int16  # for example to cast to int8.
	new_data = new_data.astype(new_dtype)
	image.set_data_dtype(new_dtype)

	# if nifty1
	if hd['sizeof_hdr'] == 348:
		new_image = nib.Nifti1Image(new_data, image.affine, header=hd)
	# if nifty2
	elif hd['sizeof_hdr'] == 540:
		new_image = nib.Nifti2Image(new_data, image.affine, header=hd)
	else:
		raise IOError('Input image header problem')

	nib.save(new_image, os.path.abspath(os.path.expanduser(image_dir)))
