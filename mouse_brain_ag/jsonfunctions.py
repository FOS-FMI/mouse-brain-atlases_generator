import json
import numpy as np
import nibabel as nib
from scipy import ndimage

#collection of functions involving reading processed json file abi annotation


def flatten_list(l):
	"""
	Flattens nested list into one list

	Parameters
	----------
	l: list 

	Returns
	-------
	List containing all items in l such that none are contained within another list
	
	Notes
	-----
	"""
	#flattens list of lists to one list
	new_l=[]
	for item in l:
		if(type(item)!=list):
			new_l.append(item)
		else:
			for nested_item in flatten_list(item):
				new_l.append(nested_item)
	return new_l

def flatten_json(l):
	"""
	flattens abi json file. extracts children structures and places on same level as parent. key "children" is now list of id numbers, not all data

	Parameters
	----------
	L: list, meant to be entered with contents under key ‘msg’ in abi json file

	Returns
	-------
	list, flattened to non-nested list of dictionaries. Tree structure is preserved as children are recorded as just a list of id numbers
	
	Notes
	-----
	"""
	#flattens abi json file. extracts children structures and places on same level as parent. key "children" is now list of id numbers, not all data
	new_l=[]

	if(len(l)>0):
		for j in range(len(l)):
			children=[]
			new_item=l[j].copy()
			flat_children=flatten_json(l[j]['children'])
			if(len(l[j]['children'])>0):
				for i in range(len(flat_children)):
					children.append(l[j]['children'][i]['id'])
					new_l.append(flat_children[i])
			new_item['children']=children
			new_l.append(new_item)

	return new_l

def find_id(idn, json_dict):
	"""Searches through json_dict (abi_annotation) to find index number of structure with id idn.

	Parameters
	----------
	idn: int, id number of a structure in ABI atlas, required
	json_dict: dict, obtained from flattened abi_annotation json file

	Returns
	-------
	returns integer, index number of structure with id idn within list json_dict['msg']. Returns NoneType if no such structure exists.

	Notes
	-----
	Only works on flattened json file
	"""
	returned_value=None
	for i in range(len(json_dict['msg'])):
		if(json_dict['msg'][i]['id']==idn):
			returned_value=i
	return returned_value

def ischildof(idn, json_dict):
	"""finds location of parent of idn in json_dict.

	Parameters
	----------
	idn: int, id number of a structure in ABI atlas, required
	json_dict: dict, obtained from flattened abi_annotation json file

	Returns
	-------
	returns integer, index number of parent of structure with id idn within list json_dict['msg']. Returns Nonetype if structure is parentless

	Notes
	-----
	Only works on flattened json file
	"""
#finds location of parent of idn in json_dict
	returned_value=None
	for i in range(len(json_dict['msg'])):
		if(idn in json_dict['msg'][i]['children']):
			returned_value=json_dict['msg'][i]['id']
	return returned_value


def trim_zeros(arr, margin=0):
	"""Removes excess rows and columns of zeros at the ends/sides of an array in any dimension numpy array.

	Parameters
	----------
	arr: numpy array, cannot be entirely zeros or will result in error
	margin: int, to leave padding of zeros and not trim all zeros. not required, default 0.

	Returns
	-------
	returns trimmed array

	Notes
	-----
	Initial code inspired by: https://stackoverflow.com/questions/55917328/numpy-trim-zeros-in-2d-or-3d
	"""
#removes extra zeros on sides of binary array so there is a smaller array to work with
#code taken from stackoverflow https://stackoverflow.com/questions/55917328/numpy-trim-zeros-in-2d-or-3d
	s = []
	for dim in range(arr.ndim):
		start = 0
		end = -1
		slice_ = [slice(None)]*arr.ndim

		go = True
		while go:
			slice_[dim] = start
			go = not np.any(arr[tuple(slice_)])
			start += 1
		start = max(start-1-margin, 0)

		go = True
		while go:
			slice_[dim] = end
			go = not np.any(arr[tuple(slice_)])
			end -= 1
		end = arr.shape[dim] + min(-1, end+1+margin) + 1

		s.append(slice(start,end))
	return arr[tuple(s)], tuple(s)


def get_target_list(idn, json_dict, flatdata):
	"""Creates a list of every id number that is present in nifti file and is contained within the structure denoted by idn. 

	Parameters
	----------
	idn: int, id of parent structure
	flatdata: numpy array, flattened one dimensional numpy array containing abi nifti labels data. Should be data of 15 micron labels

	Returns
	-------
	returns list of id number of every structure that is a child of structure idn and present in flatdata
	
	Notes
	-----
	"""

#returns list of every id number that is present in flatdata and is contained within the structure denoted by idn. children of idn is given by json_dict
	l=[]
	if(idn in flatdata):
		l.append(idn)#if idn itself is in flatdata, add to list
	elif(len(json_dict['msg'][find_id(idn,json_dict)]['children'])==0):#do nothing if idn childless
		pass
	else:
		for i in json_dict['msg'][find_id(idn,json_dict)]['children']:
			l+=get_target_list(i,json_dict, flatdata)#add children of idn to l if l has children via recursive funct
	return l

def create_binary_array(target, data):
	"""
	converts data array into binary array such that every int in list target is 1 and everything else is 0

	Parameters
	----------
	target: int, id number of structure
	data: numpy array, 3 dimensionals abi labels data

	Returns
	-------
	Binary numpy array 
	
	Notes
	-----
	"""
# converts data array into binary array such that every int in list target is 1 and everything else is 0
	a=np.isin(data, target).astype(int)
	a=trim_zeros(a, 2)[0]
	return a

def effective_resolution(arr, resolution):
	"""
	determines the effective resolution, i.e. the lowest resolution at which an object can be discerned, of a binary 3d array

	Parameters
	----------
	arr: binary numpy array, should contain labels data 
	resolution: int, resolution of labels data, i.e. 15µm, 40µm etc

	Returns
	-------
	int, effective resolution in µm
	
	Notes
	-----
	Using ndimage.binary_erosion() function performs erosion on binary array using matrices of 1s as kernels (see erosion: https://homepages.inf.ed.ac.uk/rbf/HIPR2/erode.htm)
	The largest kernel where an erosion returns a matrix with nonzero values has the dimension of the effective resolution
	For our purposes, one erosion with a large kernel is equivalent to several erosions with smaller kernels
	For example, supposing the effective resolution of a binary array is 5. An erosion with kernel of size 5x5x5 will return an array with nonzero values but a 6x6x6 kernel will 
	return a zero array. Similarly, 2 erosions with a 3x3x3 kernel will produce the same array as one erosion with a 5x5x5 kernel and 2 erosions with a 3x3x3 and 4x4x4 kernel will produce
	the same array as one erosion with a 6x6x6 kernel (relationship between these smaller kernels and the large kernel is linear)

	"""

	#initialize variables
	kernel=1
	step=10
	prev=False#prev is whether the previous erosion calculated produced a nonzero array
	prevarr=arr#prevarr is the previous array calculated
	b=arr # array that is eroded


	cont=True
	while(cont):
		#perform erosion
		b=ndimage.binary_erosion(b, structure=np.ones((step+1,step+1,step+1))).astype(arr.dtype)#when doing multiple erosions, next erosion produces result of erosion one less than sum of dimensions
		if(b.nonzero()[0].size == 0 and prev and step==1):
			#conditions of finding effective resolution dim: kernel slightly smaller returns nonzero matrix and kernel returns 0 matrix 
			finalDimension=kernel#not kernel-1 because of way adding erosions works
			cont=False
		elif(b.nonzero()[0].size == 0):#kernel-10 is nonzero. kernel is zero
			prev=False
			# Make kernel smaller by step:
			kernel=kernel-step
			#because the kernel is too big (returns zero array), we want to see the result of smaller kernel. switch previous array and b effectively "subtracts" erosion
			placeholder=b
			b=prevarr
			prevarr=placeholder
			step=1#because step is 10, make step of iteration smaller
		else:
			prev=True
			kernel=kernel+step #make kernel bigger by step
			prevarr=b #keep record of previous array
	return (finalDimension*resolution)#actual effective resolution is dim of kernel * dim of voxel

def volume_childless(ID, flatdata, resolution):
	"""
	find volume of structure with id number ID if childless

	Parameters
	----------
	ID: int, id number of structure
	flatdata: numpy array, flattened, one dimensional labels file data
	resolution: int, resolution of labels data, i.e. 15µm, 40µm etc

	Returns
	-------
	integer, volume of structures in µm^3
	
	Notes
	-----
	"""
#find volume of structure with id number ID if childless
	count=np.count_nonzero(flatdata == ID)
	return (count*(resolution**3))

def volume_branch(json_dict, ID):
	"""
	find volume of structure with id number ID if structure has children

	Parameters
	----------
	ID: int, id number of structure
	flatdata: numpy array, flattened, one dimensional labels file data
	resolution: int, resolution of labels data, i.e. 15µm, 40µm etc

	Returns
	-------
	integer, volume of structures in µm^3
	
	Notes
	-----
	"""
#find volume of structure with id number ID if has children
#recursive
	index=find_id(ID,json_dict)
	volume=0
	if len(json_dict['msg'][index]['children'])==0:
		volume=json_dict['msg'][index]['volume']
	else:
		for i in json_dict['msg'][index]['children']:
			volume+=volume_branch(json_dict,json_dict['msg'][find_id(i, json_dict)]['id'])
	return volume

def depth(json_dict):
	"""
	Finds highest depth (number of levels in tree structure) of flattened abi annotation json file

	Parameters
	----------
	json_dict: dict, obtained from flattened abi annotation json file

	Returns
	-------
	integer, depth of dictionary
	
	Notes
	-----
	"""
#find depth of abi annotation
	l=[0]
	for i in range(len(json_dict['msg'])):
		if(len(json_dict['msg'][i]['children'])==0):
			datum=json_dict['msg'][i]
			depth=1
			while(ischildof(datum['id'], json_dict)!=None):
				depth=depth+1
				datum=json_dict['msg'][find_id(ischildof(datum['id'],json_dict),json_dict)]
			l.append(depth)
	return max(l)
