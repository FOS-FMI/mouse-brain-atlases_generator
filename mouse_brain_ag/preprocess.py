import json
import os
import numpy as np
from itertools import chain as it

from jsonfunctions import ischildof, find_id, depth, flatten_list, flatten_json

#flattens abi annotation file. children of each structure stored as list of id numbers
#deletes childless structures not present in labels and adds "periphery" to children of structures in labels that have children. inspired by https://www.nature.com/articles/s41598-021-85807-0 but code not used

def run(in_file_str="abi_annotation.json", mid_file_str="abi_annotation_flattened.json", out_file_str="abi_annotation.json"):
	"""
	flattens abi annotation file (see jsonfunctions.flatten_json) and deletes childless structures not present in labels and adds "periphery" to children of structures in labels that have children

	Parameters
	----------
	in_file_str: optional, str
		file name of json file read
	mid_file_str: optional, str
		file name of json file for flattened but unaltered json, code to write file commented out
	out_file_str: optional, str
		file name for completely alrered json

	Notes
	-----
	inspired by https://www.nature.com/articles/s41598-021-85807-0 but code not used



	"""
	path = os.path.abspath('.')
	
	infile=os.path.abspath(os.path.expanduser(in_file_str))
	outfile=os.path.abspath(os.path.expanduser(out_file_str))
	midfile=os.path.abspath(os.path.expanduser(mid_file_str))

	f = open(infile,)
	data = json.load(f)
	new_id=2654 #one more than highest id in json 

	print("flattening json")
	#print(data)
	data['msg']=flatten_json(data['msg'])
	"""
	#create separate file for flattened
	with open(mid_file, 'w') as f:
		json.dump(data, f, indent=4)
	"""
	print("altering json")
	print("deleting leaf nodes with no voxels")
	print("depth",end=": ")
	print(depth(data))
	for k in range(depth(data)):#iterates over multiple times to ensure structures that have entirely voxel-less and childless children are deleted too
		for i in range(len(data['msg'])-1, -1, -1):#iterates backwards to ensure no structures are skipped over when deletes occur
			if((not data['msg'][i]['has voxels']) and (len(data['msg'][i]['children'])==0)):#childless and doesnt have voxels
				dat_id=data['msg'][i]['id']
				for j in range(len(data['msg'])-1, -1, -1):
					if(dat_id in data['msg'][j]['children']):
						data['msg'][j]['children'].remove(dat_id)
				del data['msg'][i]



	print("adjusting branch nodes with voxels")
	for i in range(len(data['msg'])-1, -1, -1):#backwards to avoid processing new peripheral structures again
		if(data['msg'][i]['has voxels'] and (len(data['msg'][i]['children'])>0)):
		#for each structure with both voxels and children. Denote voxels associated with id number as "peripheral" structure and create new data point for overall struture
			new_datum=(data['msg'][i]).copy()
			new_datum['children']=[]
			new_datum['name']=data['msg'][i]['name']+" peripheral"

			data['msg'][i]['id']=new_id
			data['msg'][i]['has voxels']=False
			if(ischildof(new_datum['id'], data)!=None):
				data['msg'][find_id(ischildof(new_datum['id'], data), data)]['children'].append(new_id)
				data['msg'][find_id(ischildof(new_datum['id'], data), data)]['children'].remove(new_datum['id'])
			data['msg'][i]['children'].append(new_datum['id'])
			data['msg'].append(new_datum)
			new_id=new_id+1

	#write to json
	with open(outfile, 'w') as f:
		json.dump(data, f, indent=4)
