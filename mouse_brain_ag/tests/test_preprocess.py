import json
import os

from ..preprocess import run
from .test_jsonfunctions import randomly_generate_annotation_structure

def test_preprocess(tmp_path):
	filename="preprocess_test_annotation.json"
	data={}
	data['msg'], IDmax=randomly_generate_annotation_structure(4, 4, min_depth=1, add_has_voxels=True)

	with open(os.path.abspath(os.path.expanduser(os.path.join(tmp_path, filename))), 'w') as f:
		json.dump(data, f, indent=4)

	in_file_str = os.path.join(tmp_path,filename)
	out_file_str = os.path.join(tmp_path,"out"+filename)
	run(in_file_str=in_file_str, out_file_str=out_file_str)

