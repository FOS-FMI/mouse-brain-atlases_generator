import random
import numpy as np

from .. import jsonfunctions

def randomly_generate_annotation_structure(max_depth, max_children, min_depth=1,add_has_voxels=False):
	depth=np.random.randint(min_depth,max_depth)
	ID=0

	list_structures=[]

	current_list=["list_structures"]

	for i in range(depth):
		new_list=[]
		for s in current_list:
			size=np.random.randint(max_children)
			if size>0:
				for j in range(size):
					if add_has_voxels:
						new_dict={
								"id": ID,
								"name": str(ID),
								"has voxels":bool(np.random.randint(2)),
								"children":[]
								}
					else:
						new_dict={
								"id": ID,
								"name": str(ID),
								"children":[]}
					exec(s+".append(new_dict)")
					ID=ID+1
					new_list.append(s+"["+str(j)+"]"+"['children']")
		current_list=new_list
	
	return list_structures, ID



def test_effective_resolution():
	xsize=np.random.randint(5000)
	ysize=np.random.randint(5000)
	zsize=np.random.randint(5000)

	array=np.random.randint(2, size=(xsize,ysize,zsize))

	return jsonfunctions.effective_resolution(array, 1)

def test_create_binary_array():
	xsize=np.random.randint(5000)
	ysize=np.random.randint(5000)
	zsize=np.random.randint(5000)

	target=[]

	for i in range(np.random.randint(20)):
		target.append(np.random.randint(10000))

	array=np.random.randint(10000, size=(xsize,ysize,zsize))

	return jsonfunctions.create_binary_array(target, array)

def test_flatten_json():
	data=randomly_generate_annotation_structure(5, 7)[0]
	return jsonfunctions.flatten_json(data)

def test_volume_childless():
	xsize=np.random.randint(5000)
	ysize=np.random.randint(5000)
	zsize=np.random.randint(5000)

	target=np.random.randint(10000)

	array=np.random.randint(10000, size=(xsize,ysize,zsize))
	flatdata=array.flatten()
	return volume_childless(target, flatdata, 1)

def test_volume_branch():
	data, maxID=randomly_generate_annotation_structure(5, 7)
	ID=np.random.randint(ID)
	json_dict={}
	json_dict["msg"]=jsonfunctions.flatten_json(data)
	return (json_dict, ID)

def test_get_target_list():
	data, maxID=randomly_generate_annotation_structure(5, 7)
	ID=np.random.randint(ID)

	xsize=np.random.randint(5000)
	ysize=np.random.randint(5000)
	zsize=np.random.randint(5000)

	array=np.random.randint(maxID, size=(xsize,ysize,zsize))
	flatdata=array.flatten()

	json_dict={}
	json_dict["msg"]=jsonfunctions.flatten_json(data)
	return get_target_list(ID, json_dict, flatdata)
