import os
import nrrd
import numpy as np
import pandas as pd
import json
import copy
from collections import OrderedDict
#from jsonpath_rw import jsonpath, parse
import nibabel as nib
from scipy import ndimage


def adjust_annotation_preliminary(
	annotation_path="abi_annotation.json",
	labels_path="abi2dsurqec_15micron_labels.nii",
	):
	"""

	Adds key 'has voxels' to each structure in abi annotation to denote if each has existing voxels in 15 micron labels. Necessary for further processing

	Parameters
	----------

	annotation_path: str, optional

	Returns
	-------
	
	Notes
	---

	annotation_path is the name of the annotation file
	"""
	
	path = os.path.abspath('.')
	image = nib.load(os.path.abspath(os.path.expanduser(labels_path)))

	# to be extra sure of not overwriting data:
	new_data = image.get_fdata()
	hd = image.header
	data_flat=new_data.flatten()

	path = os.path.abspath('.')

	annotation_file=os.path.abspath(os.path.expanduser(annotation_path))

	lines=[]
	it=0
	new_ids=0

	print("reading annotation file (preliminary)")

	with open(annotation_file) as annotation:
		for line in annotation:
			lines.append(line)
			if(line.find("\"id\": ")!=-1):
				start = line.find("\": ") + len("\": ")
				end = line.find(",")
				ID = int(line[start:end])
			if(line.find("\"hemisphere_id\": ")!=-1):
				indent=line.find("\"h")
				VC=line[:indent]+"\"has voxels\":" +hasvoxels(ID, data_flat, 10)+", \n"
				lines.append(VC)
				#it=it+1
				# print(".",end="")
				#if(it==50):
					#it=0
					#print()
		# print(new)
		# print(line)

	with open(annotation_file, "w+") as f:
		for i in lines:
			f.write(i)
