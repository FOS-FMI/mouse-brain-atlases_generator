import os
from glob import glob
import nrrd
import nibabel
import numpy as np
import sys
import math


def template(in_file_str):
	"""
	Converts nrrd template downloaded from abi to nifti format

	Parameters
	----------

	in_file_str: str
		!!! Sone description here =^.^=

	Notes
	----
	"""
	#path = os.path.dirname(sys.argv[0])
	path = os.path.abspath('.')

	in_file = os.path.abspath(os.path.expanduser(in_file_str))

	print("Reading " + in_file)
	readnrrd = nrrd.read(in_file)
	data = readnrrd[0]
	header = readnrrd[1]

	print("Converting " + in_file)

	#if space[0] == 'left':
	#	 affine_matrix[0,0] = affine_matrix[0,0] * (-1)
	#if space[1] == 'posterior':
	#affine_matrix[1,1] = affine_matrix[1,1] * (-1)

	#Units?
	affine_matrix = np.array(header["space directions"],dtype=np.float)

	affine_matrix = affine_matrix*0.001

	affine_matrix = np.insert(affine_matrix,3,[0,0,0], axis=1)
	affine_matrix = np.insert(affine_matrix,3,[0,0,0,1], axis=0)

	#Change Orientation from PIR to RAS. Steps: PIR -> RIP -> RPI -> RPS -> RAS
	data.setflags(write=1)
	data = np.swapaxes(data,0,2)
	data = np.swapaxes(data,1,2)
	data = data[:,:,::-1]
	data = data[:,::-1,:]

	img = nibabel.Nifti1Image(data,affine_matrix)

	# We just change the suffix.
	nibabel.save(img,os.path.join(path, os.path.basename(in_file).split(".")[0] + '.nii'))

	#Delete Nrrd-File
	os.remove(in_file)
