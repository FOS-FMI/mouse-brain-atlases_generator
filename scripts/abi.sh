#!/usr/bin/env bash

# Define paths:
USER=$(whoami)
WORKDIR="/var/tmp/${USER}/mouse_brain_atlases_generator/templates_and_labels"
ANNOTATION_DIR="/var/tmp/${USER}/mouse_brain_atlases_generator/annotations"
LABELS="${WORKDIR}/abi_10micron_labels.nrrd" 
LABELS_NIFTI="${WORKDIR}/abi_10micron_labels.nii"
TEMPLATE="${WORKDIR}/abi_10micron_average.nrrd"
TEMPLATE_NIFTI="${WORKDIR}/abi_10micron_average.nii"
ANNOTATION="${ANNOTATION_DIR}/abi_annotation.json"
LABELS_15="${WORKDIR}/abi2dsurqec_15micron_labels.nii"



# Make directories:
mkdir -p "${WORKDIR}"
mkdir -p "${ANNOTATION_DIR}"

echo "Downloading"
# donwload average template from ABI Website (we don't distribute this, but rather need it for the registration)
wget -O "${TEMPLATE}" http://download.alleninstitute.org/informatics-archive/current-release/mouse_ccf/average_template/average_template_10.nrrd

# download labels (we disambiguate the upstream nomenclature, labels meaning the numeric identifiers, and annotation the human-readable categorization)
wget -O "${LABELS}" http://download.alleninstitute.org/informatics-archive/current-release/mouse_ccf/annotation/ccf_2017/annotation_10.nrrd

# download annotation (we disambiguate the upstream nomenclature, labels meaning the numeric identifiers, and annotation the human-readable categorization)
wget -O "${ANNOTATION}" http://api.brain-map.org/api/v2/structure_graph_download/1.json

echo "Start processing"
python -c "from divideleftright import divide; divide(\"${LABELS}\", \"${LABELS}\", \"${ANNOTATION}\")"
python -c "from nrrd_to_nifti import template; template(\"${TEMPLATE}\")"
python -c "import conversion_and_values; conversion_and_values.labels_nrrd_to_nifti(\"${LABELS}\", \"${LABELS_NIFTI}\", \"${ANNOTATION}\")"

cp abi2dsurqec.sh "${WORKDIR}/abi2dsurqec.sh"
bash "${WORKDIR}/abi2dsurqec.sh"
rm "${WORKDIR}/abi2dsurqec.sh"

python -c "import adjust_abi2dsurqec_15labels; adjust_abi2dsurqec_15labels.adjust_15micronlabels(\"${LABELS_15}\")"
python -c "import pre_voxelcount; pre_voxelcount.adjust_annotation_preliminary(\"${ANNOTATION}\", \"${LABELS_15}\")"
python -c "import preprocess; preprocess.run(in_file_str=\"${ANNOTATION}\", out_file_str=\"${ANNOTATION}\")"
python -c "import addVCandER; addVCandER.addVCandER(in_file_str=\"${ANNOTATION}\", out_file_str=\"${ANNOTATION}\", labels_str=\"${LABELS_15}\")"
