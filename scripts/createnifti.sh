RESOLUTION=300.0

RESIZE=${RESOLUTION}/1000
USER=$(whoami)
WORKDIR="/var/tmp/${USER}/mouse_brain_atlases_generator/templates_and_labels"
ANNOTATION_DIR="/var/tmp/${USER}/mouse_brain_atlases_generator/annotations"

#WORKDIR="/home/${USER}/src/mouse-brain-atlases_generator/mouse_brain_ag/templates_and_labels"
#ANNOTATION_DIR="/home/${USER}/src/mouse-brain-atlases_generator/mouse_brain_ag/annotations"
LABELS="${WORKDIR}/abi2dsurqec_15micron_labels.nii"
ANNOTATION="${ANNOTATION_DIR}/abi_annotation_withVCandER_trial.json"
OUTPUTDIR="${WORKDIR}/abi2dsurqec_parcellated-${RESOLUTION}microns.nii"

echo "Parcellating.."
python -c "import createnifti; createnifti.create_nifti(\"${LABELS}\", \"${ANNOTATION}\", \"${OUTPUTDIR}\", ${RESOLUTION})"

echo "Resizing.."
ResampleImage 3 ${OUTPUTDIR} ${OUTPUTDIR} ${RESIZE}x${RESIZE}x${RESIZE} 0 1 4
fslorient -copyqform2sform ${OUTPUTDIR}


