#!/usr/bin/env bash
MAKE_MESH=false

while getopts ':v:n:m' flag; do
	case "${flag}" in
		v)
			PV="$OPTARG"
			;;
		n)
			PN="$OPTARG"
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

if [ -z "$PV" ]; then
	PV=9999
fi

if [ -z "$PN" ]; then
	PN="mouse-brain-atlases"
fi

P="${PN}-${PV}"
PHD="${PN}HD-${PV}"

mkdir ${P}
mkdir ${PHD}
cp FAIRUSE-AND-CITATION ${P}
cp FAIRUSE-AND-CITATION ${PHD}
pushd ${P}
	bash ../abi.sh || exit 1
	bash ../abi2dsurqec.sh || exit 1
	python ../adjustannotation.py || exit 1
	rm abi_10micron_average.nii
	rm abi_10micron_labels.nii
	# here we will need to move the larger files to the HD package directory (${PHD})
	#mv abi_15micron_average.nii ../${PHD}
	#mv abi_15micron_annotation.nii ../${PHD}
	# The name resulting from operation concatenation is verbose and confusing, here we change it:
	rm dsurqec_15micron_masked.nii
popd

tar cfJ "${P}.tar.xz" ${P}
tar cfJ "${PHD}.tar.xz" ${PHD}
str=$(sha512sum ${P}.tar.xz); echo "${str%% *}" > "${P}.sha512"
str=$(sha512sum ${PHD}.tar.xz); echo "${str%% *}" > "${PHD}.sha512"
